//
//  ColorConst.swift
//  Test_Task_News_Lampla
//
//  Created by Vlad Kosmach on 10/9/17.
//  Copyright © 2017 Vlad Zatcer. All rights reserved.
//

import UIKit

class ColorConst: UIImageView {
  public static var blueText = UIColor(hex:"00adef")
  public static var indicatorGray = UIColor(hex:"636363")
  public static var grayText = UIColor(hex:"959595")
  public static var blackText = UIColor(hex:"1e1c1d")
  public static var bgToolBar = UIColor(hex:"131313")
  public static var grayToolbarText = UIColor(hex:"a1a1a1")
}
