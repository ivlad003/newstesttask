//
//  TabPager.swift
//  Test_Task_News_Lampla
//
//  Created by Vlad Kosmach on 10/6/17.
//  Copyright © 2017 Vlad Zatcer. All rights reserved.
//



import UIKit
import Foundation
import XLPagerTabStrip


class TabPager: ButtonBarPagerTabStripViewController {

    @IBOutlet weak var buttonLeftMenu: UIBarButtonItem!
    @IBOutlet weak var buttonTitle: UIBarButtonItem!
    @IBOutlet weak var buttonSearch: UIBarButtonItem!
    lazy var searchBar : UISearchBar = UISearchBar(frame:CGRect(x: 0, y: 0, width: 270, height: 20))
    var isReload = false
    override func viewDidLoad() {
        settings.style.buttonBarBackgroundColor = ColorConst.bgToolBar
        settings.style.buttonBarItemBackgroundColor = ColorConst.bgToolBar
        settings.style.selectedBarBackgroundColor = .white
        settings.style.buttonBarItemFont = .boldSystemFont(ofSize: 14)
        settings.style.selectedBarHeight = 4.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = ColorConst.grayToolbarText
        settings.style.buttonBarItemsShouldFillAvailableWidth = true
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
        super.viewDidLoad()
        
        changeCurrentIndexProgressive = { (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = ColorConst.grayToolbarText
            newCell?.label.textColor = .white
        }
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
    }
 
    
    @IBAction func calcelClick(_ sender: Any) {
        buttonTitle.title = "Naws"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "Menu Icon"), style:  UIBarButtonItemStyle.plain, target: self, action: nil)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "Search"), style:  UIBarButtonItemStyle.plain, target: self, action: #selector(searchClick))
    }
    @IBAction func searchClick(_ sender: Any) {
        searchBar.placeholder = "Search text"
        let rightNavBarButton = UIBarButtonItem(customView:searchBar)
        self.navigationItem.leftBarButtonItem = rightNavBarButton
        buttonTitle.title = ""
        self.navigationItem.rightBarButtonItem=UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(calcelClick))
    }
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let child_1 = storyboard.instantiateViewController(withIdentifier: "someViewController") as! TabStoriesViewController
        child_1.itemInfo.title="STORIES"
        
        let child_2 = TabVideoViewController(itemInfo: "VIDEO")
        let child_3 = TabFavouritesViewController(itemInfo: "FAVOURITES")
        return [child_1, child_2, child_3]
    }
}
