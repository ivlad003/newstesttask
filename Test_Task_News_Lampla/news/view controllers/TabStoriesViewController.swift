//
//  Tab1ViewController.swift
//  Test_Task_News_Lampla
//
//  Created by Vlad Kosmach on 10/6/17.
//  Copyright © 2017 Vlad Zatcer. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import Alamofire

class TabStoriesViewController: UITableViewController, IndicatorInfoProvider{
    var itemInfo: IndicatorInfo = "STORIES"

    var models:[ItemNews] = []
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }


    override func viewDidLoad() {
        super.viewDidLoad()
        Alamofire.request("http://owledge.ru/api/v1/feedNews?lang=en&count=10&sources=7,19,13,5,15,16,12,9,10012,10010,10013,10014,10019,10018,10011&feedLineId=5").responseJSON { response in
            if let json = response.result.value {
                self.models = ItemNews.modelsFromDictionaryArray(array: json as! NSArray)
                self.tableView.reloadData()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DeviceDetailsCell", for: indexPath) as! PagerTableViewCell
            cell.models = models
            return cell
        } else {
             let cell = tableView.dequeueReusableCell(withIdentifier: "ItemWithImageCell", for: indexPath) as! ItemWithImageCell
            cell.model = models[indexPath.row-1]
            return cell
        }
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 150.0
        default:
            return 240.0
        }
    }
   
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return models.count + 1
    }
}
