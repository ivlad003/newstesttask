//
//  FireView.swift
//  Test_Task_News_Lampla
//
//  Created by Vlad Kosmach on 10/9/17.
//  Copyright © 2017 Vlad Zatcer. All rights reserved.
//

import UIKit

class FireView: UIView {
    func setup() {
        let rectShape = CAShapeLayer()
        rectShape.bounds = frame
        rectShape.position = center
        rectShape.path = UIBezierPath(roundedRect: bounds, byRoundingCorners: [.bottomLeft , .topLeft], cornerRadii: CGSize(width: 20, height: 20)).cgPath
        layer.mask = rectShape
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
