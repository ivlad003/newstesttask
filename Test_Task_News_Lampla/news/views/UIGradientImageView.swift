//
//  UIGradientImageView.swift
//  Test_Task_News_Lampla
//
//  Created by Vlad Kosmach on 10/9/17.
//  Copyright © 2017 Vlad Zatcer. All rights reserved.
//

import UIKit

class UIGradientImageView: UIImageView {
    
    let myGradientLayer: CAGradientLayer
    
   
    
    func addGradientLayer(){
        if myGradientLayer.superlayer == nil{
            self.layer.addSublayer(myGradientLayer)
        }
    }
    
    required init(coder aDecoder: NSCoder){
        myGradientLayer = CAGradientLayer()
        super.init(coder: aDecoder)!
        self.setup()
        addGradientLayer()
    }
    
    func getColors() -> [CGColor] {
        return [UIColor.clear.cgColor, UIColor(red: 0, green: 0, blue: 0, alpha: 0.8).cgColor]
    }
    
    func getLocations() -> [CGFloat]{
        return [0.5,  0.9]
    }
    
    func setup() {
        myGradientLayer.startPoint = CGPoint(x: 0.6, y: 0)
        myGradientLayer.endPoint = CGPoint(x: 0.6, y: 1)
        
        let colors = getColors()
        myGradientLayer.colors = colors
        myGradientLayer.isOpaque = false
        myGradientLayer.locations = getLocations() as [NSNumber]
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        myGradientLayer.frame = self.layer.bounds
    }
}
