//
//  PagerItem.swift
//  Test_Task_News_Lampla
//
//  Created by Vlad Kosmach on 10/9/17.
//  Copyright © 2017 Vlad Zatcer. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class PagerItem: UIView {

    @IBOutlet weak var image: UIGradientImageView!
    
    @IBOutlet weak var title: UILabel!
    
    @IBOutlet weak var resource: UILabel!
    
    @IBOutlet weak var time: UILabel!
    
    class func instanceFromNib() -> PagerItem {
        return UINib(nibName: "PagerItem", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! PagerItem
    }
    
    var model : ItemNews? = nil{
        didSet{
            Alamofire.request((model?.cover)!).responseImage { response in
                if let image = response.result.value {
                    
                    if let view = self.superview{
                        let size = CGSize(width: (view.frame.size.width), height: 150.0)
                        self.image?.image = image.af_imageAspectScaled(toFill: size)
                    }
                    
                }
            }
            title.text=model?.name
            resource.text = NSURL(string: (model?.link)!)?.host
            
            let timeInterval = Double((model?.date)!)
            let myNSDate = Date(timeIntervalSince1970: timeInterval)
            time.text = Date().offset(from:  myNSDate)
        }
    }
}
