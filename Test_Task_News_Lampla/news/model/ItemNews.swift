import Foundation

public class ItemNews {
	public var id : String?
	public var link : String?
	public var name : String?
	public var description : String?
	public var date : Int?
	public var comments : Int?
	public var views : Int?
	public var cover : String?
	public var sourceId : Int?
	public var autotop : String?
	public var platformIds : String?
	public var app_views : Int?
	public var source_views : Int?
	public var removed : String?
	public var top : String?
	public var previewId : Int?
	public var localImage : String?
	public var createdAt : String?
	public var updatedAt : String?
	public var previewLink : String?

    public class func modelsFromDictionaryArray(array:NSArray) -> [ItemNews]
    {
        var models:[ItemNews] = []
        for item in array
        {
            models.append(ItemNews(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
	required public init?(dictionary: NSDictionary) {
		id = dictionary["id"] as? String
		link = dictionary["link"] as? String
		name = dictionary["name"] as? String
		description = dictionary["description"] as? String
		date = dictionary["date"] as? Int
		comments = dictionary["comments"] as? Int
		views = dictionary["views"] as? Int
		cover = dictionary["cover"] as? String
		sourceId = dictionary["sourceId"] as? Int
		autotop = dictionary["autotop"] as? String
		platformIds = dictionary["platformIds"] as? String
		app_views = dictionary["app_views"] as? Int
		source_views = dictionary["source_views"] as? Int
		removed = dictionary["removed"] as? String
		top = dictionary["top"] as? String
		previewId = dictionary["previewId"] as? Int
		localImage = dictionary["localImage"] as? String
		createdAt = dictionary["createdAt"] as? String
		updatedAt = dictionary["updatedAt"] as? String
		previewLink = dictionary["previewLink"] as? String
	}
}
