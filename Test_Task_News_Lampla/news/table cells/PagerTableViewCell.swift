//
//  PagerTableViewCell.swift
//  Test_Task_News_Lampla
//
//  Created by Vlad Kosmach on 10/7/17.
//  Copyright © 2017 Vlad Zatcer. All rights reserved.
//

import UIKit

class PagerTableViewCell: UITableViewCell, ViewPagerDataSource{
    
@IBOutlet weak var pager: ViewPager!
    var models:[ItemNews] = []{
        didSet{
            pager.reloadData()
        }
    }
    
    
func numberOfItems(viewPager:ViewPager) -> Int {
        return models.count;
}
    
func viewAtIndex(viewPager:ViewPager, index:Int, view:UIView?) -> UIView {
        var newView = view;
    
        if(newView == nil){
            let pager : PagerItem = PagerItem.instanceFromNib()
            pager.model = models[index]
            let screenSize = UIScreen.main.bounds
            let screenWidth = screenSize.width
            pager.frame=CGRect(x: 0, y: 0, width: screenWidth-20, height: 150)
            newView = pager
        }else{
            let pager : PagerItem = newView as! PagerItem
            pager.model = models[index]
        }
        return newView!
    
 }
   
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        pager.dataSource = self
    }

}
