//
//  ItemWithImageCell.swift
//  Test_Task_News_Lampla
//
//  Created by Vlad Kosmach on 10/8/17.
//  Copyright © 2017 Vlad Zatcer. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class ItemWithImageCell: UITableViewCell {

    @IBOutlet weak var imageCell: UIImageView!
    
    
    @IBOutlet weak var title: UILabel!
    
    @IBOutlet weak var resource: UILabel!
    
    @IBOutlet weak var time: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    var model : ItemNews? = nil{
        didSet{
            Alamofire.request((model?.cover)!).responseImage { response in
                if let image = response.result.value {
                    print("image downloaded: \(image)")
                    let size = CGSize(width: self.contentView.frame.size.width, height: 150.0)
                    self.imageCell.image = image.af_imageAspectScaled(toFill: size)
                }
            }
            title.text=model?.name
            resource.text = NSURL(string: (model?.link)!)?.host
            
            let timeInterval = Double((model?.date)!)
            let myNSDate = Date(timeIntervalSince1970: timeInterval)
            time.text = Date().offset(from:  myNSDate)
        }
    }

}
