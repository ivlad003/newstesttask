//
//  ItemWithOutImageCell.swift
//  Test_Task_News_Lampla
//
//  Created by Vlad Kosmach on 10/8/17.
//  Copyright © 2017 Vlad Zatcer. All rights reserved.
//

import UIKit

class ItemWithOutImageCell: UITableViewCell {
    @IBOutlet weak var title: UILabel!
    
    @IBOutlet weak var resource: UILabel!
    
    @IBOutlet weak var time: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
